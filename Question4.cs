﻿//Import packages
using System;

//Project name
namespace Fibonnaci
{
    //Main class
    class Program
    {
        //Main method to be executed
        static void Main(string[] args)
        {
            bool proceed = true; //Set boolean to true so while loop can execute
            //Repeat until a valid integer is entered
            while(proceed)
            {
                Console.Write("Enter the value for N: "); //Request user for input
                string input = Console.ReadLine(); //Read the input
                Console.WriteLine("\nResult: ");
                Console.WriteLine("------- ");

                int n = 0;
                bool isParsable = Int32.TryParse(input, out n); //Check if the input can be parsed as an integer

                //If the input is an integer then calculate the fibonacci sequence otherwise request for input again
                if (isParsable)
                {
                    //Loop through each index until N is reached
                    for (int i = 1; i <= n; i++)
                    {
                        Console.WriteLine(i + ": " + CalculateFibonacciSequence(i)); //Display the index and the Fibonacci value
                    }
                    proceed = false; //Used to terminate the while loop on successful Fibonnaci calculation
                }
                else
                {
                    Console.WriteLine("Please enter a valid integer."); //Direct user to enter a valid integer and try again
                }
            } 
        }

        //Method to calculate the Fibonacci sequence 
        public static int CalculateFibonacciSequence(int n)
        {
            //if n is either 0 or 1 then return 0 or 1 respectively
            if ((n == 0) || (n == 1))
            {
                return n;
            }
            //otherwise calculate the Nth term recursively
            else
            {
                return CalculateFibonacciSequence(n - 1) + CalculateFibonacciSequence(n - 2);
            }
        }
    }
}