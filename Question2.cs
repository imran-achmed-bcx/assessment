﻿//Import packages
using System;

//Project name
namespace CharacterCount
{
    //Main class
    class Program
    {
        //Main method to be executed
        public static void Main(string[] args)
        {
            Console.Write("Enter String: "); 
            string input = Console.ReadLine(); //Request user to input string
            Console.WriteLine("\nResult: ");
            Console.WriteLine("------- ");

            input = input.Replace(" ", string.Empty); //Remove spaces in string (based on "Hello World output on assessment)

            //Loop through the string until no characters are left
            while (input.Length > 0)
            {
                Console.Write(input[0] + " = ");
                int count = 0; //Initialise the counter
                //Loop through the string and count the occurrences
                for (int j = 0; j < input.Length; j++)
                {
                    //Compare the first character with the rest of the characters in the string
                    if (input[0] == input[j])
                    {
                        count++; //Increment the counter as soon as a character is matched
                    }
                }
                Console.WriteLine(count); //Display the number of occurrences
                input = input.Replace(input[0].ToString(), string.Empty); //Remove the first character from the string after comparing
            }
            Console.ReadLine(); //Halt program so that the output can be viewed by the user
        }
    }
}
