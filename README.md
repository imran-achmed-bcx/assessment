# UCT Assessment

The files in the repository are the answers to the respective questions

## Question 1

The playbook first updates and then upgrades the Ubuntu servers before doing the same for the SLES servers. Once updated and upgraded, the servers are rebooted.


## Question 2
Built in checks to ensure spaces are not counted. Counts all characters in a string.

## Question 3
Download the repository from [Github](https://github.com/sds/lecture-capture.git) and then restart the NGINX service for the "worker" group of servers.

## Question 4
Calculate the Fibonacci sequence based on the Nth value. The program does not accept any non-integer value

## Question 5
Increased the memory limit in the PHP applications on a server. Also added a comment to the php.ini file on where the change was made.